library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_BIT.all;

package XTEA_PACK is
	-- States definition
	constant IDLE 				: STD_LOGIC_VECTOR(2 downto 0) := "000";
	constant BUSY_INPUT 		: STD_LOGIC_VECTOR(2 downto 0) := "001";
	constant BUSY_ENC0 			: STD_LOGIC_VECTOR(2 downto 0) := "010";
	constant BUSY_ENC1 			: STD_LOGIC_VECTOR(2 downto 0) := "011";
	constant BUSY_DEC0 			: STD_LOGIC_VECTOR(2 downto 0) := "100";
	constant BUSY_DEC1 			: STD_LOGIC_VECTOR(2 downto 0) := "101";
end XTEA_PACK;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use WORK.XTEA_PACK.all;

entity XTEA is
	port (
		clk 			: in 	STD_LOGIC;
		rst 			: in 	STD_LOGIC;

		data_input0	 	: in 	STD_LOGIC_VECTOR(31 downto 0);
		data_input1	 	: in 	STD_LOGIC_VECTOR(31 downto 0);
		data_input2	 	: in 	STD_LOGIC_VECTOR(31 downto 0);
		data_input3	 	: in 	STD_LOGIC_VECTOR(31 downto 0);
		data_input4	 	: in 	STD_LOGIC_VECTOR(31 downto 0);
		data_input5	 	: in 	STD_LOGIC_VECTOR(31 downto 0);

		data_output0 	: out 	STD_LOGIC_VECTOR(31 downto 0);
		data_output1 	: out 	STD_LOGIC_VECTOR(31 downto 0);

		input_ready 	: in 	STD_LOGIC;
		mode 			: in 	STD_LOGIC;
		output_ready 	: out 	STD_LOGIC
	);
end XTEA;

architecture BEHAVIORAL of XTEA is

	signal STATUS 		: STD_LOGIC_VECTOR(2 downto 0);
	signal NEXT_STATUS 	: STD_LOGIC_VECTOR(2 downto 0);
	signal KEY0 		: UNSIGNED(31 downto 0);
	signal KEY1 		: UNSIGNED(31 downto 0);
	signal KEY2 		: UNSIGNED(31 downto 0);
	signal KEY3 		: UNSIGNED(31 downto 0);
	signal TEXT0 		: UNSIGNED(31 downto 0);
	signal TEXT1 		: UNSIGNED(31 downto 0);
	signal COUNTER 		: UNSIGNED(5 downto 0);
	signal SUM 			: UNSIGNED(31 downto 0);

	constant DELTA 		: UNSIGNED(31 downto 0) := x"9E3779B9";
	constant ZERO		: UNSIGNED(31 downto 0) := x"00000000";
	constant ONE		: UNSIGNED(31 downto 0) := x"00000001";
	constant TWO		: UNSIGNED(31 downto 0) := x"00000002";
	constant THREE		: UNSIGNED(31 downto 0) := x"00000003";
begin
	-- FSM
	process (STATUS, input_ready)
	begin
		case STATUS is
			when IDLE =>
				if input_ready = '1' then
					NEXT_STATUS <= BUSY_INPUT;
				else
					NEXT_STATUS <= IDLE;
				end if;
			
			-- Input
			when BUSY_INPUT =>
				if mode = '0' then
					NEXT_STATUS <= BUSY_ENC0;
				elsif mode = '1' then
					NEXT_STATUS <= BUSY_DEC0;
				else
					NEXT_STATUS <= IDLE;
				end if;
			
			-- Encryption
			when BUSY_ENC0 => 
				NEXT_STATUS <= BUSY_ENC1;
			when BUSY_ENC1 => 
				if COUNTER = x"20" then
					NEXT_STATUS <= IDLE;
				else
					NEXT_STATUS <= BUSY_ENC0;
				end if;

			-- Decryption
			when BUSY_DEC0 => 
				NEXT_STATUS <= BUSY_DEC1;
			when BUSY_DEC1 => 
				if COUNTER = x"20" then
					NEXT_STATUS <= IDLE;
				else
					NEXT_STATUS <= BUSY_DEC0;
				end if;

			when others => 
				NEXT_STATUS <= STATUS;
		end case;
	end process;

	-- DATAPATH
	process (clk, rst)
		begin
			if rst = '0' then
				STATUS <= IDLE;
				data_output0 <= (others => '0');
				data_output1 <= (others => '0');
				output_ready <= '0';
				KEY0 <= (others => '0');
				KEY1 <= (others => '0');
				KEY2 <= (others => '0');
				KEY3 <= (others => '0');
				TEXT0 <= (others => '0');
				TEXT1 <= (others => '0');
				COUNTER <= (others => '0');
				SUM <= (others => '0');
			elsif clk'event and clk= '1' then
				STATUS <= NEXT_STATUS;
				case NEXT_STATUS is
					when IDLE => -- IDLE state, everything is 0 in the output ports
						data_output0 <= STD_LOGIC_VECTOR(TEXT0);
                        data_output1 <= STD_LOGIC_VECTOR(TEXT1);
                        COUNTER <= (others => '0');
                        SUM <= (others => '0');
                        output_ready <= '1';

					-- Key and text input datapath
					when BUSY_INPUT => -- Read first key
						KEY0 <= UNSIGNED(data_input0);
						KEY1 <= UNSIGNED(data_input1);
						KEY2 <= UNSIGNED(data_input2);
						KEY3 <= UNSIGNED(data_input3);
						if mode = '1' then
							SUM <= x"C6EF3720"; -- And initialize sum in case of decryption
						end if;
						TEXT0 <= UNSIGNED(data_input4);
						TEXT1 <= UNSIGNED(data_input5);
						output_ready <= '0';

					-- Below is the encryption datapath
					when BUSY_ENC0 => -- First round encryption
						case (UNSIGNED(SUM) and THREE) is
							when ZERO => 
							TEXT0 <= TEXT0 + ((((TEXT1 sll 4) xor (TEXT1 srl 5)) + TEXT1) xor (SUM + KEY0));
							when ONE => 
							TEXT0 <= TEXT0 + ((((TEXT1 sll 4) xor (TEXT1 srl 5)) + TEXT1) xor (SUM + KEY1));
							when TWO => 
							TEXT0 <= TEXT0 + ((((TEXT1 sll 4) xor (TEXT1 srl 5)) + TEXT1) xor (SUM + KEY2));
							when others => 
							TEXT0 <= TEXT0 + ((((TEXT1 sll 4) xor (TEXT1 srl 5)) + TEXT1) xor (SUM + KEY3));
						end case;
						SUM <= SUM + DELTA;
					when BUSY_ENC1 => -- Second round encryption
						case ((UNSIGNED(SUM) srl 11) and THREE) is
							when ZERO => 
							TEXT1 <= TEXT1 + ((((TEXT0 sll 4) xor (TEXT0 srl 5)) + TEXT0) xor (SUM + KEY0));
							when ONE => 
							TEXT1 <= TEXT1 + ((((TEXT0 sll 4) xor (TEXT0 srl 5)) + TEXT0) xor (SUM + KEY1));
							when TWO => 
							TEXT1 <= TEXT1 + ((((TEXT0 sll 4) xor (TEXT0 srl 5)) + TEXT0) xor (SUM + KEY2));
							when others => 
							TEXT1 <= TEXT1 + ((((TEXT0 sll 4) xor (TEXT0 srl 5)) + TEXT0) xor (SUM + KEY3));
						end case;
						COUNTER <= COUNTER + 1;

					-- Below is the decryption datapath
					when BUSY_DEC0 => -- First round decryption
						case ((UNSIGNED(SUM) srl 11) and THREE) is
							when ZERO => 
							TEXT1 <= TEXT1 - ((((TEXT0 sll 4) xor (TEXT0 srl 5)) + TEXT0) xor (SUM + KEY0));
							when ONE => 
							TEXT1 <= TEXT1 - ((((TEXT0 sll 4) xor (TEXT0 srl 5)) + TEXT0) xor (SUM + KEY1));
							when TWO => 
							TEXT1 <= TEXT1 - ((((TEXT0 sll 4) xor (TEXT0 srl 5)) + TEXT0) xor (SUM + KEY2));
							when others => 
							TEXT1 <= TEXT1 - ((((TEXT0 sll 4) xor (TEXT0 srl 5)) + TEXT0) xor (SUM + KEY3));
						end case;
						SUM <= SUM - DELTA;
					when BUSY_DEC1 => -- Second round decryption
						case (UNSIGNED(SUM) and THREE) is
							when ZERO => 
							TEXT0 <= TEXT0 - ((((TEXT1 sll 4) xor (TEXT1 srl 5)) + TEXT1) xor (SUM + KEY0));
							when ONE => 
							TEXT0 <= TEXT0 - ((((TEXT1 sll 4) xor (TEXT1 srl 5)) + TEXT1) xor (SUM + KEY1));
							when TWO => 
							TEXT0 <= TEXT0 - ((((TEXT1 sll 4) xor (TEXT1 srl 5)) + TEXT1) xor (SUM + KEY2));
							when others => 
							TEXT0 <= TEXT0 - ((((TEXT1 sll 4) xor (TEXT1 srl 5)) + TEXT1) xor (SUM + KEY3));
						end case;
						COUNTER <= COUNTER + 1;			     
						
					when others => 
						data_output0 <= STD_LOGIC_VECTOR(TEXT0);
                        data_output1 <= STD_LOGIC_VECTOR(TEXT1);
                        COUNTER <= (others => '0');
                        SUM <= (others => '0');
                        output_ready <= '1';
				end case;
			end if;
		end process;
end architecture;