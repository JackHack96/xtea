library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity XTEA_TB is
	-- Port ();
end XTEA_TB;

architecture Behavioral of XTEA_TB is

	signal clk           : STD_LOGIC;
	signal rst           : STD_LOGIC;

	signal data_input0	 : STD_LOGIC_VECTOR(31 downto 0);
    signal data_input1   : STD_LOGIC_VECTOR(31 downto 0);
    signal data_input2   : STD_LOGIC_VECTOR(31 downto 0);
    signal data_input3   : STD_LOGIC_VECTOR(31 downto 0);
    signal data_input4   : STD_LOGIC_VECTOR(31 downto 0);
    signal data_input5   : STD_LOGIC_VECTOR(31 downto 0);

	signal data_output0  : STD_LOGIC_VECTOR(31 downto 0);
	signal data_output1  : STD_LOGIC_VECTOR(31 downto 0);
	
	signal input_ready   : STD_LOGIC;
	signal mode          : STD_LOGIC;
	signal output_ready  : STD_LOGIC;
	
	signal c       : integer := 0;

	component XTEA is
		port (
            clk 			: in 	STD_LOGIC;
            rst 			: in 	STD_LOGIC;

            data_input0	 	: in 	STD_LOGIC_VECTOR(31 downto 0);
            data_input1	 	: in 	STD_LOGIC_VECTOR(31 downto 0);
            data_input2	 	: in 	STD_LOGIC_VECTOR(31 downto 0);
            data_input3	 	: in 	STD_LOGIC_VECTOR(31 downto 0);
            data_input4	 	: in 	STD_LOGIC_VECTOR(31 downto 0);
            data_input5	 	: in 	STD_LOGIC_VECTOR(31 downto 0);

            data_output0 	: out 	STD_LOGIC_VECTOR(31 downto 0);
            data_output1 	: out 	STD_LOGIC_VECTOR(31 downto 0);

            input_ready 	: in 	STD_LOGIC;
            mode 			: in 	STD_LOGIC;
            output_ready 	: out 	STD_LOGIC
		);
	end component;

begin
    xt: XTEA port map(
        clk => clk,
        rst => rst,

        data_input0 => data_input0,
        data_input1 => data_input1,
        data_input2 => data_input2,
        data_input3 => data_input3,
        data_input4 => data_input4,
        data_input5 => data_input5,

        data_output0 => data_output0,
        data_output1 => data_output1,

        input_ready   => input_ready,
        mode          => mode,
        output_ready  => output_ready
    );

    process -- clock process
    begin
        clk <= '0';
        wait for 25 ns;
        clk <= '1';
        wait for 25 ns;
    end process;

    process(clk)
    begin
        if clk'EVENT and clk = '1' then
            c <= c + 1;
            case c is
                when 0 =>
                    rst <= '0';
                    mode <= '0';
                    data_input0 <= (others => '0');
                    data_input1 <= (others => '0');
                    data_input2 <= (others => '0');
                    data_input3 <= (others => '0');
                    data_input4 <= (others => '0');
                    data_input5 <= (others => '0');
                    input_ready <= '0';
                when 1 =>
                    rst <= '1';
                    mode <= '0';
                    data_input0 <= x"6a1d78c8";
                    data_input1 <= x"8c86d67f";
                    data_input2 <= x"2a65bfbe";
                    data_input3 <= x"b4bd6e46";
                    data_input4 <= x"12345678";
                    data_input5 <= x"9abcdeff";
                    input_ready <= '1';
                when 100 =>
                    rst <= '0';
                    mode <= '0';
                    data_input0 <= (others => '0');
                    data_input1 <= (others => '0');
                    data_input2 <= (others => '0');
                    data_input3 <= (others => '0');
                    data_input4 <= (others => '0');
                    data_input5 <= (others => '0');
                    input_ready <= '0';
                when 175 =>
                    rst <= '1';
                    mode <= '1';
                    data_input0 <= x"6a1d78c8";
                    data_input1 <= x"8c86d67f";
                    data_input2 <= x"2a65bfbe";
                    data_input3 <= x"b4bd6e46";
                    data_input4 <= x"99bbb92b";
                    data_input5 <= x"3ebd1644";
                    input_ready <= '1';
                 when others =>
                    rst <= '1';
                    --mode <= '0';
                    data_input0 <= (others => '0');
                    data_input1 <= (others => '0');
                    data_input2 <= (others => '0');
                    data_input3 <= (others => '0');
                    data_input4 <= (others => '0');
                    data_input5 <= (others => '0');
                    input_ready <= '0';
            end case;
        end if;
    end process;

end Behavioral;