# XTEA

XTEA encryption algorithm for Xilinx PYNQ Z1.

The `XTEA_AXI_slave` directory contains other three directories:
- `XTEA_PYNQ`\
  It contains the final project, ready to be synthesized and deployed on the PYNQ.
  It consist of a Zynq-based block design, with the XTEA block linked through AXI bus.
- `XTEA_VHDL`\
  It contains the VHDL implementation + simulation of the XTEA block
- `XTEA_VHDL_IP`\
  It's just the XTEA IP package that's included in `XTEA_PYNQ`

Other than `XTEA_AXI_slave`, there's the `Driver` directory, which just contains a demo
Jupyter Notebook with a driver for the XTEA algorithm. It also includes a ready to use
bitstream and the `.tcl` file.